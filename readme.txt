
# Please run as follows:
python run.py runserver
sudo systemctl restart redis-server.service
celery -A contact_app.celery worker -l info
celery -A contact_app.celery beat -l info

# There's no update_time added and updates are performed even if no change to previous values
# There's some inconsistency with regards to input validation because parseargs handling doesnt completely match in-app
# handling. Marshmellow seemed overkill for the simple validation needed here.
# There might be an issue with changed emails and dangling references. Although deletes are cascaded and orphan removal
# is set, these functionalities were not tested because of time constraints
# Also I would not use the flask_restful library again, it's too simplistic and takes assumptions about what gets
# handled by the get/post/put/deleted methods that are not so straightforward. Instead I would prefer more control with
# explicit @app.route specifications
# The http codes (apart from 418) are also arguable and an explicit documentation of the API is missing.
# Also, initially I've started out with separate model and view files, only to dump the logic all together into the
# __init__.py file for simplicity. Obviously I would not do this with production code

# Run tests with python app_tests.py

# Available operations:
# GET http://localhost:5000/api/contact
# GET http://localhost:5000/api/contact/{username}
# GET http://localhost:5000/api/contact/{email}
# GET http://localhost:5000/api/contact/page{n}

# POST http://localhost:5000/api/contact
 '''{
    "emails": [
      "alex.bernell@gmail.com"
    ],
    "firstname": "Alex",
    "surname": "Bernell",
    "username": "alex_bernell"
  }'''

# PUT http://localhost:5000/api/contact/alex_bernell
 '''{
    "emails": [
      "alex.bernell@gmail.com",
      "alex.bernell2019@gmail.com"
    ],
    "firstname": "Alex",
    "surname": "Bernell",
    "username": "alex_bernell"
  }'''

# DELETE http://localhost:5000/api/contact/alex_bernell