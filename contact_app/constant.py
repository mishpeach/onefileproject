import re

ADDRESS_ALREADY_TAKEN_ = 'Email address {} is already taken.'
CONTACT_DOES_NOT_EXISTS_ = 'Contact with username {} does not yet exist, cannot update.'
USERNAME_IN_URL_DIFFERENT_ = 'Username in url different then in json body.'
USERNAME_MISSING_FROM_URL_ = 'Username must be specified in the request url for updating existing contact.'
USERNAME_ALREADY_EXISTS_ = 'Contact with username {} already exists.'
USERNAME_NOT_FOUND_ = 'Contact with username {} not found.'
EMAIL_ADDRESS_NOT_FOUND_ = 'Contact with email address {} not found.'
LIST_IS_EMPTY = 'Contact list is empty'
INVALID_POST_URL = 'Username should not be included in request url for creating new contact.'
CONTACT_EXPIRY_SECS = 60
ADDRESS_IS_NOT_VALID_ = 'Email address {} is not valid.\n'
EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")
EMAIL_MANDATORY = "Email cannot be blank.\n"
SURNAME_MANDATORY = "Surname cannot be blank.\n"
FIRSTNAME_MANDATORY = "Firstname cannot be blank.\n"
USERNAME_MANDATORY_ = "Username cannot be blank.\n"