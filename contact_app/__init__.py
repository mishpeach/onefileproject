import os
import random
from datetime import datetime, timedelta
from flask_restful import reqparse, Resource, Api
from flask import Flask, Response, abort, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from celery import Celery

from contact_app.constant import ADDRESS_ALREADY_TAKEN_, USERNAME_IN_URL_DIFFERENT_, \
    USERNAME_MISSING_FROM_URL_, USERNAME_ALREADY_EXISTS_, USERNAME_NOT_FOUND_, \
    EMAIL_ADDRESS_NOT_FOUND_, LIST_IS_EMPTY, INVALID_POST_URL, CONTACT_EXPIRY_SECS, ADDRESS_IS_NOT_VALID_, \
    EMAIL_REGEX, EMAIL_MANDATORY, SURNAME_MANDATORY, FIRSTNAME_MANDATORY, USERNAME_MANDATORY_

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Contact(db.Model):
    username = db.Column(db.String(64), primary_key=True)
    firstname = db.Column(db.String(64))
    surname = db.Column(db.String(64))
    emails = db.relationship("Email", cascade="all,delete-orphan", backref="owner", lazy='dynamic')
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<Contact %s>' % self.username


class Email(db.Model):
    email_address = db.Column(db.String(64), primary_key=True)
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    contact_id = db.Column(db.String, db.ForeignKey('contact.username'))

    def __repr__(self):
        return '<Email %s>' % self.email_address


# db.init_app(app)
db.drop_all()
db.create_all()

parser = reqparse.RequestParser(bundle_errors=True)
parser.add_argument('username', type=str, location='json', required=True, help=USERNAME_MANDATORY_)
parser.add_argument('firstname', type=str, location='json', required=True, help=FIRSTNAME_MANDATORY)
parser.add_argument('surname', type=str, location='json', required=True, help=SURNAME_MANDATORY)
parser.add_argument('emails', type=list, action='append', location='json', required=True, help=EMAIL_MANDATORY)


class ContactApi(Resource):
 
    def get(self, id=None, page=1):
        if not id:
            contacts = Contact.query.paginate(page, 10).items
            if not contacts:
                abort(418, description=LIST_IS_EMPTY)
        else:
            if '@' in id:
                email = Email.query.get(id)
                if not email:
                    abort(404, description=EMAIL_ADDRESS_NOT_FOUND_.format(id))
                contacts = [Contact.query.get(email.owner.username)]
                # still check if empty in case of dangling email
            else:
                contacts = [Contact.query.get(id)]

            if not contacts[0]:
                abort(404, description=USERNAME_NOT_FOUND_.format(id))


        result = {}
        for contact in contacts:
            result[contact.username] = {
                'username': contact.username,
                'firstname': contact.firstname,
                'surname': contact.surname,
                'emails': [email.email_address for email in contact.emails]
            }
        return jsonify(result)

    def post(self, id=None):
        if id:
            abort(400, description=INVALID_POST_URL)
        args = parser.parse_args()
        validation_error = self._validate(args)
        if validation_error:
            abort(400, description=validation_error)

        username = args['username']
        firstname = args['firstname']
        surname = args['surname']
        emails = args['emails']

        contact = Contact.query.get(username)
        if contact:
            abort(400, description=USERNAME_ALREADY_EXISTS_.format(username))

        for email_address in emails:
            email_address = ''.join(email_address)
            email = Email.query.filter_by(email_address=email_address).first()
            if email:
                abort(400, description=ADDRESS_ALREADY_TAKEN_.format(email_address))

        contact = Contact(username=username, firstname=firstname, surname=surname)
        for email_address in emails:
            email = Email(email_address=''.join(email_address), owner=contact)
            db.session.add(email)

        db.session.add(contact)
        db.session.commit()

        response = Response(status=201)
        return response

    def put(self, id=None):
        if not id:
            abort(400, description=USERNAME_MISSING_FROM_URL_)
        args = parser.parse_args()
        validation_error = self._validate(args)
        if validation_error:
            abort(Response(validation_error))

        username = args['username']
        if id != username:
            abort(400, description=USERNAME_IN_URL_DIFFERENT_)

        firstname = args['firstname']
        surname = args['surname']
        emails = args['emails']

        contact = Contact.query.get(username)
        if not contact:
            abort(400, description=USERNAME_ALREADY_EXISTS_.format(username))

        current_emails = Email.query.filter_by(owner=contact).all()
        if current_emails:
            for current_email in current_emails:
                db.session.delete(current_email)

        for email in emails:
            email = ''.join(email)
            email_entity = Email.query.get(email)
            if email_entity and email_entity.contact_id != contact.username:
                abort(400, description=ADDRESS_ALREADY_TAKEN_.format(email))
            else:
                email_entity = Email(email_address=email, owner=contact)
                db.session.add(email_entity)

        contact.username, contact.firstname, contact.surname = username, firstname, surname

        db.session.commit()

        response = Response(status=204)
        return response

    def delete(self, id):
        contact = Contact.query.get(id)
        if contact:
            #contact.delete()
            db.session.delete(contact)
            db.session.commit()
            return Response(status=200)
        else:
            return Response(status=204)

    def _validate(self, args):
        emails = args['emails']
        firstname = args['firstname']
        surname = args['surname']
        username = args['username']
        validation_error = ''
        validation_error += constant.USERNAME_MANDATORY if username == '' else ''
        validation_error += constant.FIRSTNAME_MANDATORY if firstname == '' else ''
        validation_error += constant.SURNAME_MANDATORY if surname == '' else ''
        if not emails:
            validation_error += EMAIL_MANDATORY
        for email in emails:
            email = ''.join(email)
            if not EMAIL_REGEX.fullmatch(email): validation_error += ADDRESS_IS_NOT_VALID_.format(email)
        return validation_error


api = Api(app)
api.add_resource(ContactApi, '/api/contact',
                 '/api/contact/page<int:page>',
                 '/api/contact/<string:id>')

app.debug = True
app.config['DEBUG'] = True
app.config['LOG_FILE'] = 'application.log'
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379',
    CELERYBEAT_SCHEDULE={
        'create_random_contact': {
            'task': 'contact_app.create_random_contact',
            'schedule': timedelta(seconds=15)
        },
        'delete_old_contacts': {
            'task': 'contact_app.delete_old_contacts',
            'schedule': timedelta(seconds=60)
        },
    }
)

if not app.debug:
    import logging
    from logging import FileHandler

    file_handler = FileHandler(app.config['LOG_FILE'])
    file_handler.setLevel(logging.INFO)
    # file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))


def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        # abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

celery = make_celery(app)


def read_random_line(f, filesize):
    offset = random.randrange(filesize)
    f.seek(offset)  # go to random position
    f.readline()  # discard - bound to be partial line
    random_line = f.readline()  # bingo!
    # extra to handle last/first line edge cases
    if len(random_line) == 0:  # we have hit the end
        f.seek(0)
        random_line = f.readline()  # so we'll grab the first line instead
    return random_line.strip()

firstnames_size = os.stat('firstnames.csv').st_size
lastnames_size = os.stat('lastnames.csv').st_size
firstnames = open('firstnames.csv')
lastnames = open('lastnames.csv')


@celery.task()
def create_random_contact():
    print('Running create_random_contact')
    # with app.app_context():
    firstname = read_random_line(firstnames, firstnames_size)
    lastname = read_random_line(lastnames, lastnames_size)
    number = random.randint(2000, 2020)
    random_contact = Contact(username=(firstname + '_' + lastname).lower(), firstname=firstname, surname=lastname)
    email_part = (firstname + '.' + lastname).lower()
    email1 = Email(email_address=email_part + '@gmail.com', owner=random_contact)
    email2 = Email(email_address=email_part + str(number) + '@gmail.com', owner=random_contact)

    db.session.add(random_contact)
    db.session.add(email1)
    db.session.add(email2)
    db.session.commit()
    print('Contact ' + firstname + ' ' + lastname + ' created')


@celery.task()
def delete_old_contacts():
    print('Running delete_old_contacts..')
    contacts = Contact.query.filter(Contact.create_time <= datetime.utcnow() - timedelta(seconds=CONTACT_EXPIRY_SECS)).all()
    if contacts:
        for contact in contacts:
            try:
                contact.delete()
            except:
                print(type(contact))
        db.session.commit()
        print(str(len(contacts)) + ' contact(s) deleted.')
