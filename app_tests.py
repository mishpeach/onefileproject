import os
import json
import unittest2 as unittest
import tempfile
from contact_app import app, db
from contact_app import constant
#nosetests app_tests:ContactTestCase.test_contacts

JSON_HEADER_ = {'Content-Type': 'application/json'}

class ContactTestCase(unittest.TestCase):

    def setUp(self):
        self.test_db_file = tempfile.mkstemp()[1]
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + self.test_db_file
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        os.remove(self.test_db_file)

    def test_home(self):
        rv = self.app.get('/')
        self.assertEqual(rv.status_code, 404)

    def test_get_contacts(self):
        rv = self.app.get('/api/contact')
        self.assertEqual(rv.status_code, 418)
        self.assertTrue(json.loads(rv.data)['message'] == constant.LIST_IS_EMPTY)

        # Try to get first page
        rv = self.app.get('/api/contact/page1')
        self.assertEqual(rv.status_code, 418)

        # Try to get first page
        rv = self.app.get('/api/contact/page2')
        self.assertEqual(rv.status_code, 404)
        # 404 is from flask alchemy paginate logic
        contacts = []
        for i in range(15):
            mryl_dup = {
                "emails": [
                    "myrl.vento" + str(i) + "@gmail.com",
                    "myrl.vento2001" + str(i) + "@gmail.com"
                ],
                "firstname": "Myrl" + str(i),
                "surname": "Vento" + str(i),
                "username": "myrl_vento" + str(i)
            }
            contacts.append(mryl_dup)
        # Add one contact
        rv = self.app.post('/api/contact', data=json.dumps(contacts[0]), headers=JSON_HEADER_)
        rv = self.app.get('/api/contact')
        self.assertEqual(rv.status_code, 200)
        # Check that contact list contains added contact
        self.assertTrue(json.loads(rv.data)['myrl_vento0'] == contacts[0])

        # Add second contact
        rv = self.app.post('/api/contact', data=json.dumps(contacts[1]), headers=JSON_HEADER_)
        rv = self.app.get('/api/contact')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(json.loads(rv.data)['myrl_vento1'] == contacts[1])

        # Try to get second page (of 10) contacts
        rv = self.app.get('/api/contact/page2')
        self.assertEqual(rv.status_code, 404)
        # 404 is returned by flask alchemy paginate logic

        # Get first page of contacts
        rv = self.app.get('/api/contact/page1')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(len(json.loads(rv.data)) == 2)

        # Add the rest of the contacts
        for i in range(2,15):
            rv = self.app.post('/api/contact', data=json.dumps(contacts[i]), headers=JSON_HEADER_)

        # Get first, now full page of contacts
        rv = self.app.get('/api/contact/page1')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(len(json.loads(rv.data)) == 10)

        # Get first page with implicit page parameter
        rv = self.app.get('/api/contact')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(len(json.loads(rv.data)) == 10)

        # Get second page of contacts
        rv = self.app.get('/api/contact/page2')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(len(json.loads(rv.data)) == 5)

        # Get contact by email address
        rv = self.app.get('/api/contact/myrl.vento0@gmail.com')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue('myrl.vento0@gmail.com' in json.loads(rv.data)['myrl_vento0']['emails'])

        # Try to get contact by unregistered email address
        rv = self.app.get('/api/contact/myrl.vento999@gmail.com')
        self.assertEqual(rv.status_code, 404)
        self.assertTrue(json.loads(rv.data)['message'] == constant.EMAIL_ADDRESS_NOT_FOUND_.format('myrl.vento999@gmail.com'))

        # Get contact via username
        rv = self.app.get('/api/contact/myrl_vento0')
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(json.loads(rv.data)['myrl_vento0']['username'] == 'myrl_vento0')

        # Get contact for unregistered username
        rv = self.app.get('/api/contact/myrl_vento999')
        self.assertEqual(rv.status_code, 404)
        self.assertTrue(json.loads(rv.data)['message'] == constant.USERNAME_NOT_FOUND_.format('myrl_vento999'))

    def test_update_contact(self):
        herschel_trentham = {
            "emails": [
                "herschel.trentham@gmail.com",
                "herschel.trentham2015@gmail.com"
            ],
            "firstname": "Herschel",
            "surname": "Trentham",
            "username": "herschel_trentham"
        }
        rv = self.app.post('/api/contact', data=json.dumps(herschel_trentham), headers=JSON_HEADER_)

        # Change surname
        herschel_trentham['surname'] = "Trentham Baumann"
        rv = self.app.put('/api/contact/'+ herschel_trentham['username'], data=json.dumps(herschel_trentham), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 204)

        # Call put without username in url
        rv = self.app.put('/api/contact', data=json.dumps(herschel_trentham), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.USERNAME_MISSING_FROM_URL_)

        # Call put with username in url different from json body username
        herschel_trentham['username'] = "herschel_trentham_cannot_change"
        rv = self.app.put('/api/contact/herschel_trentham', data=json.dumps(herschel_trentham), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.USERNAME_IN_URL_DIFFERENT_)
        herschel_trentham['username'] = "herschel_trentham"

        herschel_trentham['emails'][1] = "herschel.trentham2019@gmail.com"
        rv = self.app.put('/api/contact/'+ herschel_trentham['username'], data=json.dumps(herschel_trentham), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 204)

        # get contact to see changes
        rv = self.app.get('/api/contact/' + herschel_trentham['username'])
        self.assertEqual(rv.status_code, 200)
        self.assertTrue(json.loads(rv.data)[herschel_trentham['username']]['surname'] == herschel_trentham['surname'])
        self.assertTrue(json.loads(rv.data)[herschel_trentham['username']]['emails'][1] == herschel_trentham['emails'][1])

        # try to change contact with email address used by another contact
        jae_cuadra = {
            "emails": [
                "jae.cuadra@gmail.com",
                "jae.cuadra2007@gmail.com"
            ],
            "firstname": "Jae",
            "surname": "Cuadra",
            "username": "jae_cuadra"
        }
        rv = self.app.post('/api/contact', data=json.dumps(jae_cuadra), headers=JSON_HEADER_)
        herschel_trentham['emails'][1] = jae_cuadra['emails'][1]
        rv = self.app.put('/api/contact/' + herschel_trentham['username'], data=json.dumps(herschel_trentham),
                          headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.ADDRESS_ALREADY_TAKEN_.format(jae_cuadra['emails'][1]))

    def test_delete_contact(self):
        tana_trueheart = {
            "emails": [
                "tana.trueheart@gmail.com",
                "tana.trueheart2010@gmail.com"
            ],
            "firstname": "Tana",
            "surname": "Trueheart",
            "username": "tana_trueheart"
        }
        rv = self.app.post('/api/contact', data=json.dumps(tana_trueheart), headers=JSON_HEADER_)
        rv = self.app.delete('/api/contact/tana_trueheart', headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 200)
        rv = self.app.delete('/api/contact/tana_trueheart', headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 204)


    def test_create_contact(self):
        new_contact = {
            "emails": [
              "laquita.double@gmail.com",
              "laquita.double2015@gmail.com"
            ],
            "firstname": "Laquita",
            "surname": "Double",
            "username": "laquita_double"
        }
        # Create new contact
        rv = self.app.post('/api/contact', data=json.dumps(new_contact), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 201)
        # Try to create same contact again
        rv = self.app.post('/api/contact', data=json.dumps(new_contact), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.USERNAME_ALREADY_EXISTS_.format('laquita_double'))
        # Try to create contact with username also given in url
        rv = self.app.post('/api/contact' + '/' + new_contact['username'], data=json.dumps(new_contact), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.INVALID_POST_URL)
        # Create new contact having one email address matching that of another contact
        contact_with_email_taken = {
            "emails": [
              "anamaria.schueth@gmail.com",
              "anamaria.schueth2008@gmail.com",
              "laquita.double2015@gmail.com"
            ],
            "firstname": "Anamaria",
            "surname": "Schueth",
            "username": "anamaria_schueth"
        }
        rv = self.app.post('/api/contact', data=json.dumps(contact_with_email_taken), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] == constant.ADDRESS_ALREADY_TAKEN_.format('laquita.double2015@gmail.com'))
        # Create contact without email address
        contact_without_email = {
            "emails": [
            ],
            "firstname": "Xiomara",
            "surname": "Bugna",
            "username": "xiomara_bugna"
        }
        rv = self.app.post('/api/contact', data=json.dumps(contact_without_email), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] ['emails']== constant.EMAIL_MANDATORY)
        del(contact_without_email['emails'])
        rv = self.app.post('/api/contact', data=json.dumps(contact_without_email), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message'] ['emails']== constant.EMAIL_MANDATORY)
        # Create contact with invalid email address
        contact_with_invalid_email = {
            "emails": [
              "adrien.steinbrook@gmail@com",
              "adrien.steinbrook200?@gmail.com"
            ],
            "firstname": "Adrien",
            "surname": "Steinbrook",
            "username": "adrien_steinbrook"
        }
        rv = self.app.post('/api/contact', data=json.dumps(contact_without_email), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(json.loads(rv.data)['message']['emails'] == constant.EMAIL_MANDATORY)
        # Create contact without firstname and surname
        contact_without_names = {
            "emails": [
                "lashaun.patin@gmail.com",
                "lashaun.patin2010@gmail.com"
            ],
            "firstname": "lashaun",
            "username": "lashaun_patin"
        }
        rv = self.app.post('/api/contact', data=json.dumps(contact_without_names), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(constant.SURNAME_MANDATORY in json.loads(rv.data)['message']['surname'])

        contact_without_names['surname'] = ''
        contact_without_names['firstname'] = ''
        rv = self.app.post('/api/contact', data=json.dumps(contact_without_names), headers=JSON_HEADER_)
        self.assertEqual(rv.status_code, 400)
        self.assertTrue(constant.SURNAME_MANDATORY in json.loads(rv.data)['message'])
        self.assertTrue(constant.FIRSTNAME_MANDATORY in json.loads(rv.data)['message'])



if __name__ == '__main__':
    unittest.main()